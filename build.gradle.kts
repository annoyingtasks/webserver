import buildVersions.PluginVersion
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
    implementation("de.comhix.commons:updater:${buildVersions.DependencyVersions.commonsVersion}")
    implementation("de.comhix.commons:logging:${buildVersions.DependencyVersions.commonsVersion}")
    implementation(project(":shared-code"))
    implementation("com.google.guava:guava:31.1-jre")
    implementation("com.google.code.gson:gson:2.9.0")
    implementation("io.ktor:ktor-client-core:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("io.ktor:ktor-client-cio:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("io.ktor:ktor-server-html-builder:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("io.ktor:ktor-server-core:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("io.ktor:ktor-server-netty:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("io.ktor:ktor-server-content-negotiation:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("io.ktor:ktor-server-cors:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("io.ktor:ktor-server-status-pages:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("io.ktor:ktor-server-sessions:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("io.ktor:ktor-serialization:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("io.ktor:ktor-serialization-kotlinx-json:${buildVersions.DependencyVersions.ktorVersion}")
    implementation("org.litote.kmongo:kmongo:${buildVersions.DependencyVersions.kmongoVersion}")
    implementation("org.litote.kmongo:kmongo-coroutine:${buildVersions.DependencyVersions.kmongoVersion}")
    implementation("org.litote.kmongo:kmongo-serialization:${buildVersions.DependencyVersions.kmongoVersion}")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${buildVersions.DependencyVersions.kotlinSerializationVersion}")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:${buildVersions.DependencyVersions.kotlinTimeVersion}")
    implementation("org.mnode.ical4j:ical4j:3.2.3")
    implementation("ch.qos.logback:logback-classic:1.2.11")
    implementation("io.insert-koin:koin-core:${buildVersions.DependencyVersions.koinVersion}")
    testImplementation("org.amshove.kluent:kluent:${buildVersions.DependencyVersions.kluentVersion}")
    testImplementation("io.mockk:mockk:${buildVersions.DependencyVersions.mockkVersion}")
    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-junit"))
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:${buildVersions.DependencyVersions.kotlinCoroutineVersion}")
    testImplementation("io.insert-koin:koin-test:${buildVersions.DependencyVersions.koinVersion}")
    testImplementation("org.litote.kmongo:kmongo-flapdoodle:${buildVersions.DependencyVersions.kmongoVersion}")
}

plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    id("jacoco")
    id("org.jetbrains.dokka")
}

jacoco.toolVersion = PluginVersion.jacoco

sourceSets {
    main {
        java {
            srcDir("${project.projectDir}/src/generated/kotlin")
        }
    }
}

tasks.compileKotlin {
    kotlinOptions.jvmTarget = PluginVersion.javaVersion.toString()
}
tasks.compileTestKotlin {
    kotlinOptions.jvmTarget = PluginVersion.javaVersion.toString()
}

java {
    sourceCompatibility = PluginVersion.javaVersion
    targetCompatibility = PluginVersion.javaVersion
}

tasks.test {
    useJUnit()
}

val copyToLib by tasks.creating(Copy::class) {
    from(configurations.compileClasspath)
    into("$buildDir/output/lib")
}

tasks.jar {
    dependsOn(copyToLib)
    manifest {
        attributes["Main-Class"] = "${project.group}.${project.name}.MainKt"
        attributes["Class-Path"] = configurations.compileClasspath.get().joinToString(" ") { "lib/${it.name}" }

        attributes["Implementation-Version"] = project.version
    }
    destinationDirectory.set(file("$buildDir/output"))
    archiveFileName.set("application.${archiveExtension.get()}")
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(false)
        csv.required.set(false)
        html.outputLocation.set(file("${buildDir}/reports/jacocoHtml"))
    }
}

tasks.check {
    dependsOn(tasks.jacocoTestReport)
}

val generateSources: Task by tasks.creating {
    outputs.dir("${project.projectDir}/src/generated/kotlin")

    doFirst {
        generateFile(
            "${project.group}.${project.name}.info",
            "Version.kt",
            """object Version{
    const val BUILD_VERSION = "${project.version}"
    const val BUILD_DATE = "${Date()}"
    const val BUILD_TIMESTAMP = ${Date().time}
}
"""
        )
    }
}

fun generateFile(packageName: String, fileName: String, fileContent: String) {
    val outputDir: File = file("${project.projectDir}/src/generated/kotlin/${packageName.replace(".", "/")}")
    if (!outputDir.exists()) outputDir.mkdirs()

    Files.write(
        Paths.get(outputDir.absolutePath, fileName), """package $packageName

$fileContent
""".toByteArray()
    )
}

tasks.compileKotlin {
    dependsOn(generateSources)
}

val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
    destinationDirectory.set(file("$buildDir/output"))
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
    destinationDirectory.set(file("$buildDir/output"))
}

val runJar by tasks.registering(Exec::class) {
    dependsOn(tasks.jar)
    group = "execution"
    commandLine("java", "-jar", tasks.jar.get().archiveFile.get().asFile.canonicalPath)
}