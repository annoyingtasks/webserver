package de.comhix.tasks.webserver.db

import de.comhix.commons.logging.info
import de.comhix.tasks.webserver.KoinTestBase
import de.comhix.tasks.webserver.dashboard.Dashboard
import org.amshove.kluent.`should be equal to`
import org.litote.kmongo.findOneById
import org.litote.kmongo.getCollection

/**
 * @author Benjamin Beeker
 */
class DashboardTest : KoinTestBase() {

    // https://issueantenna.com/repo/flapdoodle-oss/de.flapdoodle.embed.process/issues/132
    //    @Test
    fun `load dashboard with calendar`() {
        info { "load dashboard with calendar" }

        // given
        val dashboard = Dashboard(widgets = listOf())
        mongoDatabase.getCollection<Dashboard>().insertOne(dashboard)

        // when
        val loaded = mongoDatabase.getCollection<Dashboard>().findOneById(dashboard.id)

        // then
        loaded `should be equal to` dashboard
    }
}