package de.comhix.tasks.webserver.db

import de.comhix.commons.logging.info
import de.comhix.tasks.webserver.KoinTestBase
import de.comhix.tasks.webserver.dashboard.CalendarData
import de.comhix.tasks.webserver.dashboard.WidgetData
import org.amshove.kluent.`should be equal to`
import org.litote.kmongo.findOneById
import org.litote.kmongo.getCollection

/**
 * @author Benjamin Beeker
 */
class WidgetDataTest : KoinTestBase() {

    // https://issueantenna.com/repo/flapdoodle-oss/de.flapdoodle.embed.process/issues/132
    //    @Test
    fun `load calendar data`() {
        info { "load calendar data" }

        // given
        val calendarData = CalendarData(calendarUrls = listOf("123", "456"))
        mongoDatabase.getCollection<WidgetData>().insertOne(calendarData)

        // when
        val loaded = mongoDatabase.getCollection<WidgetData>().findOneById(calendarData.id)

        // then
        loaded `should be equal to` calendarData
    }
}