package de.comhix.tasks.webserver

import de.comhix.commons.logging.info
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import kotlinx.coroutines.runBlocking

/**
 * @author Benjamin Beeker
 */
class MainTest {
    //    @Test
    fun `test dashboard`() {
        runBlocking {
            info { "test dashboard" }

            // given
            val client = HttpClient(CIO)

            // when
            val response = client.get("http://localhost:8080/widget/62c5dd6905cf0d6d88d26624/data")

            // then
            val bodyAsText = response.bodyAsText()
            info { bodyAsText }
        }
    }
}