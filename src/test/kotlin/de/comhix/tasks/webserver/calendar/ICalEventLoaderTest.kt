package de.comhix.tasks.webserver.calendar

import de.comhix.commons.logging.info
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.`should not be empty`
import java.time.Period
import kotlin.test.Test

/**
 * @author Benjamin Beeker
 */
class ICalEventLoaderTest {
    private val loader = object : ICalEventLoader() {}

    @Test
    fun `get events`() {
        info { "get events" }

        // when
        val events = runBlocking {
            loader.getNextEvents(
                "https://calendar.google.com/calendar/ical/de.german%23holiday%40group.v.calendar.google.com/public/basic.ics",
                Period.ofDays(365)
            )
        }

        // then
        events.`should not be empty`()
    }
}