package de.comhix.tasks.webserver.calendar

import de.comhix.commons.logging.info
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.amshove.kluent.`should not be empty`
import kotlin.test.Test
import kotlin.time.Duration.Companion.days

/**
 * @author Benjamin Beeker
 */
class USBCalendarTest {
//    @Test
    fun `test load usb calendar`() = runTest {
        info { "test load usb calendar" }

        // given
        val calendar = USBCalendar(mockk(relaxed = true))

        // when
        val loaded = calendar.loadUsbDates(365.days)

        // then
        loaded.forEach {
            info { "${it.start} ${it.name}" }
        }
        loaded.`should not be empty`()
    }
}