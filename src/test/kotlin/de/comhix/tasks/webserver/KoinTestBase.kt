package de.comhix.tasks.webserver

import com.mongodb.client.MongoDatabase
import de.comhix.tasks.webserver.config.TestKoinModule
import org.junit.Before
import org.koin.core.Koin
import org.koin.core.component.inject
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.module.Module
import org.koin.test.KoinTest

/**
 * @author Benjamin Beeker
 */
abstract class KoinTestBase(private val module: Module = TestKoinModule.module) : KoinTest {

    protected val mongoDatabase: MongoDatabase by inject()

    override fun getKoin(): Koin {
        stopKoin()
        return startKoin {
            modules(module)
        }.koin
    }

    @Before
    fun `clean up database`() {
        mongoDatabase.drop()
    }
}
//
//private val koin: Koin by lazy {
//    startKoin {
//        modules(TestKoinModule.module)
//    }.koin
//}
