package de.comhix.tasks.webserver.info

import org.amshove.kluent.`should not be null`
import java.util.logging.Logger
import kotlin.test.Test

/**
 * @author Benjamin Beeker
 */
class VersionTest {
    companion object {
        private val LOG = Logger.getLogger(VersionTest::class.java.name)!!
    }

    @Test
    fun `Version should be available`() {
        LOG.info("Version should be available")

        Version.BUILD_VERSION.`should not be null`()
    }
}