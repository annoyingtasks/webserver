package de.comhix.tasks.webserver.config

import de.comhix.tasks.webserver.calendar.USBCalendar
import io.mockk.mockk
import org.koin.dsl.module
import org.litote.kmongo.KMongo
import org.litote.kmongo.defaultMongoTestVersion

/**
 * @author Benjamin Beeker
 */
object TestKoinModule {
    val module = module {
        includes(KoinModule.module)
        single { mockk<USBCalendar>() }
        single {
            StandaloneEmbeddedMongo(defaultMongoTestVersion)
        }
        single {
            val mongoPort = get<StandaloneEmbeddedMongo>().port
            KMongo.createClient("mongodb://localhost:$mongoPort")
        }
    }
}