package de.comhix.tasks.webserver.config

import de.flapdoodle.embed.mongo.MongodProcess
import de.flapdoodle.embed.mongo.MongodStarter
import de.flapdoodle.embed.mongo.config.MongodConfig
import de.flapdoodle.embed.mongo.config.Net
import de.flapdoodle.embed.mongo.distribution.IFeatureAwareVersion
import de.flapdoodle.embed.process.runtime.Network

internal class StandaloneEmbeddedMongo(private val version: IFeatureAwareVersion) {

    val port: Int
        get() = process.port

    private val process: Process by lazy {
        val port = Network.freeServerPort(Network.getLocalHost())
        Process(port, createInstance(port))
    }

    private fun createInstance(port: Int): MongodProcess {
        val config: MongodConfig = MongodConfig.builder()
            .version(version)
            .net(Net(port, Network.localhostIsIPv6()))
            .build()

        return MongodStarter.getDefaultInstance().prepare(config).let { executable ->
            Runtime.getRuntime().addShutdownHook(object : Thread() {
                override fun run() {
                    executable.stop()
                }
            })
            executable.start()
        }
    }

    private data class Process(val port: Int, val mongodProcess: MongodProcess)
}