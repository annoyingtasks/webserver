package de.comhix.tasks.webserver

import de.comhix.commons.logging.warn
import de.comhix.tasks.webserver.config.Config
import de.comhix.tasks.webserver.config.KoinModule
import de.comhix.tasks.webserver.info.Version
import org.koin.core.context.startKoin
import java.util.logging.LogManager
import java.util.logging.Logger
import kotlin.system.exitProcess

/**
 * @author Benjamin Beeker
 */
fun main(args: Array<String>) {
    if (args.contains("--version")) {
        println(Version.BUILD_VERSION)
        exitProcess(0)
    }

    LogManager.getLogManager().readConfiguration(Config::class.java.getResourceAsStream("/logging.properties"))
    val log = Logger.getGlobal()

    val koin = startKoin {
        modules(KoinModule.dbModule, KoinModule.module)
    }.koin

    log.info("creating http server")

    val server: Server = koin.get()

    log.info("starting http server")

    log.fine("Fine logging is enabled")
    log.warning("server started - version ${Version.BUILD_VERSION} built ${Version.BUILD_DATE}")
    server.startServer(blocking = true)
}