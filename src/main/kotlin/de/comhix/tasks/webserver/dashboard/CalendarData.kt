package de.comhix.tasks.webserver.dashboard

import kotlinx.serialization.*
import org.bson.types.ObjectId

@Serializable
data class CalendarData(
    @SerialName("_id") @Contextual val id: ObjectId = ObjectId(),
    val calendarUrls: List<String>
) : WidgetData