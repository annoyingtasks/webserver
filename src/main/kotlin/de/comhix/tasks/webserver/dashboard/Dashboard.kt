package de.comhix.tasks.webserver.dashboard

import de.comhix.tasks.shared.data.Widget
import kotlinx.serialization.*
import org.bson.types.ObjectId
import de.comhix.tasks.shared.data.Dashboard as SharedDashboard

/**
 * @author Benjamin Beeker
 */
@Serializable
data class Dashboard(
    @SerialName("_id") @Contextual val id: ObjectId = ObjectId(),
    val widgets: List<Widget>
) {
    fun asShared() = SharedDashboard(widgets)
}