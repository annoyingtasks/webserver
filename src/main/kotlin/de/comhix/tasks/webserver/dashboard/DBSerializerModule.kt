package de.comhix.tasks.webserver.dashboard

import kotlinx.serialization.modules.SerializersModule

/**
 * @author Benjamin Beeker
 */
object DBSerializerModule {
    val module = SerializersModule {
        polymorphic(WidgetData::class, CalendarData::class, CalendarData.serializer())
    }
}