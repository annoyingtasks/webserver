package de.comhix.tasks.webserver.user

import kotlinx.serialization.*
import org.bson.types.ObjectId

/**
 * @author Benjamin Beeker
 */
@Serializable
data class User(@SerialName("_id") @Contextual val id: ObjectId = ObjectId(), val name: String)