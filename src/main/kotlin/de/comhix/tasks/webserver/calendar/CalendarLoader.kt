package de.comhix.tasks.webserver.calendar

import de.comhix.tasks.shared.data.CalendarData
import de.comhix.tasks.shared.data.CalendarEntry
import java.time.Period
import java.time.ZoneId
import kotlin.time.Duration.Companion.days

/**
 * @author Benjamin Beeker
 */
class CalendarLoader(private val usbCalendar: USBCalendar) : ICalEventLoader() {

    val ZONE: ZoneId = ZoneId.of("Europe/Berlin")

    suspend fun loadCalendarData(calendarUrls: List<String>, period: Period = Period.ofDays(6)): CalendarData {
        val periodIntoNextDay = period.plusDays(1)
        val events: List<CalendarEntry> = calendarUrls.flatMap { this.getNextEvents(it, periodIntoNextDay) }.map { it.toCalendarEntry() }

        val usbEvents = usbCalendar.loadUsbDates(period.days.days + (period.months * 30).days + (period.years * 365).days)

        return CalendarData(period.days + 1, events + usbEvents)
    }
}