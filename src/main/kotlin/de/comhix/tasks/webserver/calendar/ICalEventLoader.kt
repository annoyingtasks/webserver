package de.comhix.tasks.webserver.calendar

import de.comhix.tasks.shared.data.CalendarEntry
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsChannel
import io.ktor.utils.io.jvm.javaio.toInputStream
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.filter.predicate.PeriodRule
import net.fortuna.ical4j.model.*
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.property.Summary
import net.fortuna.ical4j.model.property.Uid
import net.fortuna.ical4j.util.MapTimeZoneCache
import java.io.InputStream
import java.time.Period as JavaTimePeriod

/**
 * @author Benjamin Beeker
 */
abstract class ICalEventLoader {
    companion object {
        init {
            System.setProperty("net.fortuna.ical4j.timezone.cache.impl", MapTimeZoneCache::class.java.name)
        }
    }

    open suspend fun loadICal(calendarUrl: String): InputStream = HttpClient(CIO).get(calendarUrl).bodyAsChannel().toInputStream()

    suspend fun getNextEvents(calendarUrl: String, period: JavaTimePeriod): List<ICalCalenderEntry> {
        val builder = CalendarBuilder()

        val calendar: Calendar = withContext(Dispatchers.IO) {
            val calendarStream = loadICal(calendarUrl)
            builder.build(calendarStream)
        }

        val calendarPeriod = Period(DateTime(), period)
        val rule = PeriodRule<VEvent>(calendarPeriod)
        return calendar.getComponents<VEvent>(Component.VEVENT)
            .filter { event -> rule.test(event) }
            .flatMap { event ->
                event.calculateRecurrenceSet(calendarPeriod)
                    .map { eventPeriod ->
                        ICalCalenderEntry(
                            event.getProperty<Uid>(Property.UID).value,
                            event.getProperty<Summary>(Property.SUMMARY).value,
                            Instant.fromEpochMilliseconds(eventPeriod.start.time),
                            Instant.fromEpochMilliseconds(eventPeriod.end.time)
                        )
                    }
            }
    }
}

@Serializable
data class ICalCalenderEntry(val uid: String, val title: String, val startTime: Instant, val endTime: Instant) {
    fun toCalendarEntry() = CalendarEntry(title, startTime, endTime)
}