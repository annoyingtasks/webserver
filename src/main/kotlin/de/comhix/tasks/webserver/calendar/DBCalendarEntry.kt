package de.comhix.tasks.webserver.calendar

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * @author Benjamin Beeker
 */
@Serializable
data class DBCalendarEntry(@SerialName("_id") val id: String, val entry: ICalCalenderEntry)