package de.comhix.tasks.webserver.calendar

import com.mongodb.client.MongoCollection
import de.comhix.commons.logging.*
import de.comhix.tasks.shared.data.CalendarEntry
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import io.ktor.http.Parameters
import kotlinx.datetime.Clock
import org.litote.kmongo.find
import java.io.InputStream
import java.time.Period
import kotlin.time.Duration

/**
 * @author Benjamin Beeker
 */
class USBCalendar(private val collection: MongoCollection<DBCalendarEntry>) : ICalEventLoader() {

    suspend fun loadUsbDates(duration: Duration): List<CalendarEntry> {
        val entries = loadFromDb(duration) ?: queryFromWeb()

        return entries.map { it.entry.toCalendarEntry() }
    }

    private fun loadFromDb(duration: Duration): List<DBCalendarEntry>? {

        val criteria =
            """{
            "${DBCalendarEntry::entry.name}.${ICalCalenderEntry::endTime.name}": {${Dollar}gte:"${Clock.System.now()}"}
        }""".trimIndent()

        info { "query: $criteria" }

        val dbEntries = collection.find(criteria).toList()
        return dbEntries.ifEmpty {
            null
        }?.filter { Clock.System.now().plus(duration) >= it.entry.startTime }
    }

    private suspend fun queryFromWeb(): List<DBCalendarEntry> {
        val events = getNextEvents(UsbUrl, Period.ofDays(365))

        val dbEntries = events.map {
            DBCalendarEntry(it.uid, it)
        }
        dbEntries.forEach {
            try {
                collection.insertOne(it)
            }
            catch (e: Exception) {
                error(e) { "could not save calendar entry" }
            }
        }

        return dbEntries
    }

    override suspend fun loadICal(calendarUrl: String): InputStream {
        val httpClient = HttpClient(CIO)

        val steps: List<Step> = listOf(
            Step(
                Pair("mm_aus_ort.x", (Math.random() * 60).toInt().toString()),
                Pair("mm_aus_ort.y", (Math.random() * 60).toInt().toString())
            ),
            Step(
                Pair("mm_frm_str_name", "Markstraße"),
                Pair("mm_aus_str_txt_submit", "suchen"),
                Pair("xxx", "1")
            ),
            Step(
                Pair("xxx", "1"),
                Pair("mm_frm_hnr_sel", "44799;Wiemelhausen;348;"),
                Pair("mm_aus_hnr_sel_submit", "weiter"),
            ),
            Step(
                Pair("xxx", "1"),
                Pair("mm_ica_auswahl", "iCalendar-Datei")
            ),
            Step(
                Pair("xxx", "1"),
                Pair("mm_frm_type", "termine"),
                Pair("mm_frm_fra_RM", "RM"),
                Pair("mm_frm_fra_PPK", "PPK"),
                Pair("mm_frm_fra_DSD", "DSD"),
                Pair("mm_ica_gen", "iCalendar-Datei laden"),
            )
        )

        var body = httpClient.get(calendarUrl).bodyAsText()

        steps.forEach {
            val session = getSession(body)
            val httpResponse = httpClient.submitForm(calendarUrl, formParameters = Parameters.build {
                it.parameter.forEach {
                    append(it.first, it.second)
                }
                append("mm_ses", session)
            })

            body = httpResponse.bodyAsText()
        }

        return body.byteInputStream()
    }

    private fun getSession(body: String): String {
        val result = SessionRegex.find(body)
        if (result == null) {
            info { body }
        }
        return result!!.groupValues[1]
    }

    companion object{
        private const val UsbUrl = "https://www.muellmax.de/abfallkalender/usb/res/UsbStart.php"

        private val SessionRegex = Regex("<input type=\"hidden\" name=\"mm_ses\" value=\"(.*)\".*?>")

        private const val Dollar = "\$"
    }

    private class Step(vararg val parameter: Pair<String, String>)
}