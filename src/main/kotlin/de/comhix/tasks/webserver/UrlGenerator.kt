package de.comhix.tasks.webserver

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.get
import io.ktor.http.URLBuilder
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.nio.file.Files
import java.nio.file.Paths
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */

suspend fun main() {
    val json = Files.readString(Paths.get("urls.json"))
    val urlData: UrlData = Json { ignoreUnknownKeys = true }.decodeFromString(json)
    val builder = URLBuilder(urlData.baseUrl)
    urlData.calendarUrls.forEach { builder.parameters.append("calendarUrl", it) }

    val url = builder.build()
    val httpResponse = HttpClient(CIO).get(url)
    val responseCode = httpResponse.status.value

    val logger = Logger.getLogger("main")
    logger.info { "url: $url" }
    logger.info { "responseCode: $responseCode" }
}

@Serializable
private data class UrlData(val baseUrl: String, val calendarUrls: List<String>)
