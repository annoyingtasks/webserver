package de.comhix.tasks.webserver.config

import com.google.gson.Gson
import java.io.InputStreamReader
import java.nio.file.Files
import java.nio.file.Paths

/**
 * @author Benjamin Beeker
 */
object ApplicationConfig : Config {
    private val PATH = Paths.get("config.json")

    private val config by lazy { load() }

    override val shutdownToken: String
        get() = config.shutdownToken

    override val mongo: MongoConfig
        get() = config.mongo

    private fun load(): Config {
        return Gson().fromJson(InputStreamReader(Files.newInputStream(PATH)), ConfigContainer::class.java)
    }
}

private data class ConfigContainer(override val mongo: MongoConfig, override val shutdownToken: String) : Config

interface Config {
    val mongo: MongoConfig
    val shutdownToken: String
}

data class MongoConfig(val username: String, val password: String, val database: String, val host: String, val port: Int)