package de.comhix.tasks.webserver.config

import com.mongodb.client.*
import de.comhix.tasks.webserver.Server
import de.comhix.tasks.webserver.api.DashboardApi
import de.comhix.tasks.webserver.api.WidgetApi
import de.comhix.tasks.webserver.calendar.CalendarLoader
import de.comhix.tasks.webserver.calendar.USBCalendar
import de.comhix.tasks.webserver.dashboard.DBSerializerModule
import org.koin.core.scope.Scope
import org.koin.dsl.module
import org.litote.kmongo.KMongo
import org.litote.kmongo.getCollection
import org.litote.kmongo.serialization.registerModule

/**
 * @author Benjamin Beeker
 */
object KoinModule {
    val module = module {
        single {
            registerModule(DBSerializerModule.module)
            val client = get<MongoClient>()
            client.getDatabase(ApplicationConfig.mongo.database)
        }

        single { Server(get(), get()) }
        single { USBCalendar(getCollection()) }
        single { WidgetApi(getCollection(), get()) }
        single { DashboardApi(getCollection()) }
        single { CalendarLoader(get()) }
    }
    val dbModule = module {
        single { KMongo.createClient("mongodb://${ApplicationConfig.mongo.username}:${ApplicationConfig.mongo.password}@${ApplicationConfig.mongo.host}:${ApplicationConfig.mongo.port}") }
    }

    private inline fun <reified T : Any> Scope.getCollection(): MongoCollection<T> {
        return get<MongoDatabase>().getCollection()
    }
}