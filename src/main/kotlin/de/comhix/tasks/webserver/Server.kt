package de.comhix.tasks.webserver

import de.comhix.commons.logging.info
import de.comhix.tasks.shared.data.SharedCodeJsonModule
import de.comhix.tasks.webserver.api.DashboardApi
import de.comhix.tasks.webserver.api.WidgetApi
import de.comhix.tasks.webserver.routings.*
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.json
import io.ktor.server.application.install
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.server.netty.NettyApplicationEngine
import io.ktor.server.plugins.contentnegotiation.ContentNegotiation
import io.ktor.server.plugins.cors.routing.CORS
import io.ktor.server.plugins.statuspages.StatusPages
import io.ktor.server.response.respond
import io.ktor.server.routing.routing
import kotlinx.serialization.json.Json

/**
 * @author Benjamin Beeker
 */
class Server(private val dashboardApi: DashboardApi, private val widgetApi: WidgetApi) {
    fun startServer(port: Int = 8080, blocking: Boolean = true): NettyApplicationEngine {
        return embeddedServer(Netty, port = port) {
            install(ContentNegotiation) {
                json(Json {
                    serializersModule = SharedCodeJsonModule.module
                })
            }
            install(StatusPages) {
                exception<HttpException> { call, it ->
                    info(it) { "${it.message}" }
                    call.respond(HttpStatusCode(it.status, it.reason), it.reason)
                }
                exception<Throwable> { call, it ->
                    //                    error(it) { "unknown error: ${it.message}" }
                    call.respond(HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.description)
                    throw it
                }
            }
            install(CORS) {
                allowHeader("Access-Control-Allow-Headers")
                allowHeader("Origin")
                allowHeader("Accept")
                allowHeader("X-Requested-With")
                allowHeader("Content-Type")
                allowHeader("Access-Control-Request-Method")
                allowHeader("Access-Control-Request-Headers")
                allowHeader("Authorization")
                allowHeader("Cookie")

                IntRange(1, UShort.MAX_VALUE.toInt()).forEach {
                    allowHost("localhost:$it")
                    allowHost("127.0.0.1:$it")
                }
                allowHost("tasks.nozo.tv", schemes = listOf("https"))

                allowCredentials = true
            }
            routing {
                controlRouting()
                staticRouting()
                dashboardRouting(dashboardApi)
                widgetRouting(widgetApi)
            }
        }.start(blocking)
    }
}