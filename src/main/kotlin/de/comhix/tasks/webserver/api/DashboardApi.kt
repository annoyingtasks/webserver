package de.comhix.tasks.webserver.api

import com.mongodb.client.MongoCollection
import de.comhix.tasks.shared.api.DashboardApi
import de.comhix.tasks.shared.data.Dashboard
import de.comhix.tasks.webserver.routings.HttpException
import org.bson.types.ObjectId
import org.litote.kmongo.findOneById
import de.comhix.tasks.webserver.dashboard.Dashboard as DBDashboard

/**
 * @author Benjamin Beeker
 */
class DashboardApi(private val collection: MongoCollection<DBDashboard>) : DashboardApi {
    override suspend fun get(id: String): Dashboard {
        return collection.findOneById(ObjectId(id))?.asShared() ?: throw HttpException(404, "Dashboard not found")
    }
}