package de.comhix.tasks.webserver.api

import com.mongodb.client.MongoCollection
import de.comhix.tasks.shared.api.WidgetApi
import de.comhix.tasks.shared.data.WidgetData
import de.comhix.tasks.webserver.calendar.CalendarLoader
import de.comhix.tasks.webserver.dashboard.CalendarData
import de.comhix.tasks.webserver.routings.HttpException
import org.bson.types.ObjectId
import org.litote.kmongo.findOneById
import de.comhix.tasks.webserver.dashboard.WidgetData as DBWidgetData

/**
 * @author Benjamin Beeker
 */
class WidgetApi(private val collection: MongoCollection<DBWidgetData>, private val calendarLoader: CalendarLoader) : WidgetApi {
    override suspend fun getData(id: String): WidgetData {
        val widgetData = collection.findOneById(ObjectId(id)) ?: throw HttpException(404, "Dashboard not found")

        return if (widgetData is CalendarData) {
            calendarLoader.loadCalendarData(widgetData.calendarUrls)
        }
        else {
            object : WidgetData {}
        }
    }
}