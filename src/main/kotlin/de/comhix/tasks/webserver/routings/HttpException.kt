package de.comhix.tasks.webserver.routings

/**
 * @author Benjamin Beeker
 */
data class HttpException(val status: Int, val reason: String) : RuntimeException("$status - $reason")