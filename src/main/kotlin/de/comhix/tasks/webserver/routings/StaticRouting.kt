package de.comhix.tasks.webserver.routings

import io.ktor.server.application.call
import io.ktor.server.response.respondFile
import io.ktor.server.routing.Routing
import io.ktor.server.routing.get
import java.io.File

/**
 * @author Benjamin Beeker
 */
private val BASE_DIR = File("../static-web-content/public/static")

fun Routing.staticRouting() {
    get("/static/{file}") {
        val fileName = call.parameters["file"]!!
        call.respondFile(BASE_DIR, fileName)
    }
}