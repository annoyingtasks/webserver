package de.comhix.tasks.webserver.routings

import de.comhix.tasks.webserver.api.WidgetApi
import io.ktor.server.application.call
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get

/**
 * @author Benjamin Beeker
 */
fun Route.widgetRouting(widgetApi: WidgetApi) {
    get("widget/{widgetId}/data") {
        val widgetIdString = call.parameters["widgetId"]!!

        call.respond(
            widgetApi.getData(widgetIdString)
        )
    }
}