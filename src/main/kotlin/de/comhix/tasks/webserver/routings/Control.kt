package de.comhix.tasks.webserver.routings

import de.comhix.commons.logging.info
import de.comhix.tasks.webserver.config.ApplicationConfig
import io.ktor.server.application.call
import io.ktor.server.routing.Route
import io.ktor.server.routing.post
import kotlin.system.exitProcess

/**
 * @author Benjamin Beeker
 */
fun Route.controlRouting() {
    post("control/shutdown") {
        val token = call.request.headers["token"]
        if (token == ApplicationConfig.shutdownToken) {
            info { "shutting down command from ${call.request.local.remoteHost} with token '$token'" }
            Thread {
                Thread.sleep(1000L)
                exitProcess(0)
            }.start()
        }
        else {
            info { "shutdown command from ${call.request.local.remoteHost} with illegal token '$token'" }
        }
    }
}