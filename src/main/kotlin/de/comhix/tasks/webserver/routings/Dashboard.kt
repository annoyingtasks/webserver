package de.comhix.tasks.webserver.routings

import de.comhix.tasks.webserver.api.DashboardApi
import io.ktor.server.application.call
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get

/**
 * @author Benjamin Beeker
 */

fun Route.dashboardRouting(dashboardApi: DashboardApi) {
    get("dashboard/{dashboardId}") {
        val dashboardIdString = call.parameters["dashboardId"]!!

        call.respond(
            dashboardApi.get(dashboardIdString)
        )
    }
}